/**
 * 
 */
package easycrypto;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;
import java.util.Vector;

import org.junit.jupiter.api.*;

/**
 
 This is test class for EasyCryptoAPI class. 
 JUnit API documentation: https://junit.org/junit5/docs/current/api/
 
 @author Antti Männikkö
 */
class EasyCryptoAPITest {
	
	/**
	Test the methods() function
	 */
	@Test
	void testMethods() {
		assertNotNull(EasyCryptoAPI.methods(), "testMethods failure: function methods() returns null");
	}
	
	/**
    Test the encrypt and decrypt methods with a random String
     */
	@Test 
	void testEncryptAndDecrypt() {
		Vector<String> m = CryptoMethodFactory.getMethods();
		EasyCryptoAPI.Result encryptResult = null;
		EasyCryptoAPI.Result decryptResult = null;
        String random = UUID.randomUUID().toString();
		for (String s : m) {
			encryptResult = EasyCryptoAPI.encrypt(random, s);
			assertEquals(encryptResult.resultCode(),EasyCryptoAPI.ResultCode.ESuccess, 
				"testEncryptAndDecrypt encrypt failure: ResultCode " + encryptResult.resultCode());
			assertNotNull(encryptResult.result(), "testEncryptAndDecrypt failure: encryption" + s + " returns null String");
			decryptResult = EasyCryptoAPI.decrypt(encryptResult.result(), s);
			assertEquals(decryptResult.resultCode(),EasyCryptoAPI.ResultCode.ESuccess, 
				"testEncryptAndDecrypt decrypt failure: ResultCode " + decryptResult.resultCode());
			assertNotNull(decryptResult.result(), "testEncryptAndDecrypt failure: decryption" + s + " returns null String");
			assertEquals(decryptResult.result(), random, 
				"testEncryptAndDecrypt failure: decryptResult " + decryptResult.result() + "Does not match with" + random);
		}
	}

}
